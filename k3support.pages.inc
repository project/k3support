<?php

function k3support_center() {
	$tickets = k3support_get_tickets('new');
	$data = array();
	foreach($tickets as $key => $ticket) {
		$data[] = array(
			'date'		=>	format_date($ticket->created),
			'title'		=>	l($ticket->title, 'node/'.$ticket->nid),
		);
	}
	$header = array(
		t('Date'),
		t('Title'),
	);
	$output = theme_table($header, $data);
	return $output;
}

?>