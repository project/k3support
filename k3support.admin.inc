<?php

/*
 * K3Support admin settings
 */

function k3support_settings_form() {
	$form = array();
	
	$form['roles'] = array(
		'#type'			=>	'fieldset',
		'#title'		=>	t('Roles'),
		'#collapsible'	=>	TRUE,
		'#tree'			=>	TRUE,
	);
	
	$form['roles']['support'] = array(
		'#type'			=>	'select',
		'#title'		=>	t('Support team role'),
		'#options'		=>	user_roles(),
		'#default_value'=>	variable_get('k3support_support_team_role', 0),
	);
	
	$form['roles']['reporter'] = array(
		'#type'			=>	'select',
		'#title'		=>	t('Reporter role'),
		'#options'		=>	user_roles(),
		'#default_value'=>	variable_get('k3support_reporter_role', 0),
	);
	
	$vocab_priority = array();
	foreach(taxonomy_get_vocabularies() as $vocab) {
		$vocab_priority[$vocab->vid] = $vocab->name;
	}
	
	$form['vocabulary'] = array(
		'#type'			=>	'fieldset',
		'#title'		=>	t('Vocabulary'),
		'#collapsible'	=>	TRUE,
		'#collapsed'	=>	TRUE,
		'#tree'			=>	TRUE,
	);
	
	/*
	 
	$form['vocabulary']['info'] = array(
		'#type'			=>	'markup',
		'#value'		=>	t('Visit the taxonomy settings page to create new vocabularies for each category'),	
	);
	*/
	
	$form['vocabulary']['priority'] = array(
		'#type'			=>	'select',
		'#title'		=>	t('Select vocabulary for support priority'),
		'#options'		=>	$vocab_priority,
	);
	
	$form['submit'] = array(
		'#type'			=>	'submit',
		'#value'		=>	t('Save'),
	);
	
	return $form;
}

function k3support_settings_form_submit($form_id, $form_state) {
	variable_set('k3support_support_team_role', $form_state['values']['roles']['support']);
	variable_set('k3support_reporter_role', $form_state['values']['roles']['reporter']);
}

?>